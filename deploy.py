import getpass
import os
import subprocess
import sys
import traceback

from manage import main

main()
print(chr(27) + "[2J")
print(chr(27) + "[1;1f")
from django.contrib.auth import get_user_model

ERRORS = []
BASE_DIR = os.path.dirname(os.path.abspath(__file__))


class PRINT:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    @classmethod
    def warning(cls, *args):
        print(cls.WARNING, end='')
        print(*args)
        print(cls.ENDC)

    @classmethod
    def error(cls, *args):
        print(cls.FAIL + cls.BOLD, end='')
        print(*args, end=cls.ENDC + '\n')

    @classmethod
    def info(cls, *args):
        print(cls.OKBLUE, end='')
        print(*args, end=cls.ENDC + '\n')

    @classmethod
    def exception(cls, message, err=None):
        try:
            cls.error(message)
            raise Exception(message)
        except Exception as e:
            ERRORS.append(e if err is None else err)


def run():
    if 'linux' not in sys.platform:
        raise Exception("This code runs on Linux only.")

    PRINT.warning('Be sure to run this script in a virtualenv')

    # Check OS environment variables
    for var in ['DJANGO_SECRET_KEY', 'ALLOWED_HOST', 'TEMPLATE_CONSTANT_HOST', 'RECAPCHA_SECRET_KEY',
                'RECAPCHA_SITE_KEY']:
        if var not in os.environ:
            PRINT.exception('Missing env variable', var)

    # Check for ENV variable for debug env
    if os.getenv("DJANGO_DEBUG") == '1':
        PRINT.warning('DEBUG IS ON')

    # Django migrations
    PRINT.info('Performing Django migrations...')
    p = subprocess.Popen(["python3", os.path.join(BASE_DIR, "manage.py"), "makemigrations"], stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT)
    for line in iter(p.stdout.readline, b''):
        print('>>> {}'.format(line.decode('utf-8').rstrip()))

    p = subprocess.Popen(["python3", os.path.join(BASE_DIR, "manage.py"), "migrate"], stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT)
    for line in iter(p.stdout.readline, b''):
        print('>>> {}'.format(line.decode('utf-8').rstrip()))

    # Django Static Files
    PRINT.info('Collect static files...')
    p = subprocess.Popen(["python3", os.path.join(BASE_DIR, "manage.py"), "collectstatic"], stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT)
    for line in iter(p.stdout.readline, b''):
        print('>>> {}'.format(line.decode('utf-8').rstrip()))

    # Deploy check
    PRINT.info('Performing Django deploy check...')
    result = subprocess.run(["python3", os.path.join(BASE_DIR, "manage.py"), "check", "--deploy"], capture_output=True)
    print(result.stdout.decode('utf-8'), "\n")
    if result.stderr.decode('utf-8'):
        print(result.stderr.decode('utf-8'))
        while True:
            yn = str(input("\nDo you wish to continue? [y/n]: ")).lower().strip()
            if len(yn) == 1:
                if yn[0] != 'y':
                    PRINT.exception('Cancelled')
                break

    # Superuser
    PRINT.info('Checking for superuser account')
    User = get_user_model()
    if not User.objects.filter(is_superuser=True).exists():
        PRINT.info('Create superuser:')
        username = str(input('Username: '))
        email = str(input('Email: '))
        password = getpass.getpass()
        user = User.objects.create_superuser(username=username, email=email, password=password)

    for exc in ERRORS:
        traceback.print_exception(type(exc), exc, exc.__traceback__)

    if len(ERRORS) != 0:
        PRINT.error(len(ERRORS), 'errors. Cannot continue.')
        exit()

    PRINT.info('Complete. Setup/restart nginx, and setup/restart wsgi/asgi service')


if __name__ == '__main__':
    run()
