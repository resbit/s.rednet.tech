from django.contrib import admin
from django.utils.html import escape, mark_safe

from .models import Redir

admin.site.site_header = 'Redirect Administration'


@admin.register(Redir)
class RedirAdmin(admin.ModelAdmin):
    list_display = ('short_link', 'get_long_link', 'created_at', 'expire_at')
    search_fields = ('short_link', 'long_link')
    list_filter = ('expire_at', 'created_at')

    def get_long_link(self, obj):
        link = obj.long_link
        return mark_safe(f'<a href="{link}">{escape(link)}</a>')

    get_long_link.short_description = 'Long link'
    get_long_link.admin_order_field = 'long_link'
