import os

from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'main'

    def ready(self):
        if os.getenv("DJANGO_DEBUG") == '1':
            print('\033[93mWARNING: IN DEBUG MODE!\033[0m')
