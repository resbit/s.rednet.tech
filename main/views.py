import json
import os
import random
import string

import requests
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator, RegexValidator
from django.db.models import Q
from django.http import HttpResponseRedirect, JsonResponse, HttpResponseServerError
from django.shortcuts import render
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.http import require_POST

from .models import Redir


class Index(View):
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
            'recaptcha_site_key': os.environ.get('RECAPCHA_SITE_KEY'),
            'host': os.environ['TEMPLATE_CONSTANT_HOST'],
            'random_example': ''.join(random.choices(string.ascii_lowercase + string.digits, k=random.randint(4, 6)))
        })


class Redirect(View):
    template_name = 'error.html'

    def get(self, request, redir_path, *args, **kwargs):
        redir_obj = Redir.objects.filter(
            Q(short_link__exact=redir_path) & (
                    Q(expire_at__isnull=True) | Q(expire_at__gt=timezone.now())
            )
        )

        if redir_obj.exists():
            return HttpResponseRedirect(redir_obj.first().long_link)

        return render(request, self.template_name, context={
            'message': 'Resource could not be found',
            'host': os.environ['TEMPLATE_CONSTANT_HOST']
        })


@method_decorator(require_POST, name='dispatch')
class Create(View):
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)

        # ReCapcha Verify
        resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data={
            'response': data.get('gRecaptchaResponse'),
            'secret': os.environ['RECAPCHA_SECRET_KEY']
        }, headers={
            'content-type': 'application/x-www-form-urlencoded'
        })

        recaptcha_response = resp.json()

        if recaptcha_response.get('success'):
            # Check if input URL is valid
            val = URLValidator()
            try:
                val(data['long_link'])
            except ValidationError as e:
                return JsonResponse({'Response': 'Malformed URL (long link), please try again'})

            # Check custom url is valid and not already in DB
            if data['short_link'] != '':
                val = RegexValidator(regex='^[A-Za-z0-9]{4,16}$')
                try:
                    val(data['short_link'])
                except ValidationError as e:
                    return JsonResponse({'Response': 'Malformed custom URL (short link), please try again'})

                if Redir.objects.filter(short_link=data['short_link']).exists():
                    return JsonResponse({'Response': 'The custom URL already exists, please try another one'})
            else:
                data['short_link'] = Redir.gen_short_link()

            # Check expire time is good
            try:
                data['expiration'] = Redir.expire_lookup(index=data['expiration'])
            except IndexError:
                return JsonResponse({'Response': 'The expiration specified is not valid'})

            # Save new entry to DB
            new_entry = Redir(long_link=data['long_link'], short_link=data['short_link'], expire_at=data['expiration'])
            new_entry.save()

            if not Redir.objects.filter(short_link=data['short_link']).exists():
                return HttpResponseServerError("Internal server error")

            return JsonResponse({
                'Response': 'Success! Link expires: ' + (
                    data['expiration'].strftime('%a %F at %l:%M %p %Z') if data['expiration'] is not None else 'Never'),
                'Link': os.environ['TEMPLATE_CONSTANT_HOST'] + '/' + data['short_link']})

        else:
            return JsonResponse({'Response': 'Failed ReCaptcha, please try again.'})
