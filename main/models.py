import uuid
from datetime import timedelta

from django.db import models
from django.utils import timezone


class Redir(models.Model):
    short_link = models.CharField(max_length=20, unique=True)
    long_link = models.URLField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    expire_at = models.DateTimeField(blank=True, null=True)

    # TODO: Statistics

    @classmethod
    def expire_lookup(cls, duruation=None, index=None):
        expire_duration_options = [
            ('ne', 'Never', None),
            ('1d', 'In 1 Day', timedelta(days=1)),
            ('1w', 'In 1 Week', timedelta(weeks=1)),
            ('2w', 'In 2 Weeks', timedelta(weeks=2)),
            ('1m', 'In 1 Month', timedelta(days=31)),
        ]
        if duruation is not None and index is not None:
            raise Exception('Cannot call function')
        elif duruation is not None:
            for item in expire_duration_options:
                if duruation == item[0]:
                    return timezone.now() + item[2] if duruation != 'ne' else item[2]
            return IndexError
        elif index is not None:
            if index >= len(expire_duration_options):
                raise IndexError
            return timezone.now() + expire_duration_options[index][2] if index != 0 else expire_duration_options[0][2]

    @classmethod
    def gen_short_link(cls):
        while True:
            short_link = uuid.uuid4().hex[:7]
            if not cls.objects.filter(short_link__exact=short_link).exists():
                return short_link
